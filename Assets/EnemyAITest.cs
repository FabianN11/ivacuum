﻿using UnityEngine;
using System.Collections;

public class EnemyAITest : MonoBehaviour {

	public float walkSpeed = 2.0f;
	public float wallLeft = 0.0f;
	public float wallRight = 5.0f;
	float walkingDirection = 1.0f;
	float playerDirection;
	public Transform player;
	public bool follow = false;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		GetComponent<Rigidbody2D>().velocity = new Vector2 (walkingDirection * walkSpeed, GetComponent<Rigidbody2D>().velocity.y);

		if (follow == true) {
            //Move towards target     
            GetComponent<Rigidbody2D>().velocity = new Vector2 (playerDirection *(walkSpeed+1), GetComponent<Rigidbody2D>().velocity.y);
			
		} else 

		if ((walkingDirection > 0.0f) && (transform.position.x > wallRight)) {
			walkingDirection *= -1;
            Flip();
		} else if ((walkingDirection < 0.0f) && (transform.position.x < wallLeft)) {
			walkingDirection *= -1;
            Flip();
		}

        if (Input.GetKeyDown(KeyCode.R))
        {
            FlipY();
        }
    }

    void Flip()
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void FlipY()
    {
        Vector3 theScale = transform.localScale;
        theScale.y *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter2D(Collider2D coll) {
		Debug.Log("gotcha");

		if (coll.tag == "Player") {
			//Vector2.Distance(transform.position.x, player.transform.position.x);
			follow = true;


		}
	}

	void OnTriggerStay2D(Collider2D coll) {
		if (player.position.x > this.transform.position.x) {
			playerDirection = 1.0f;
        } else if (player.position.x < this.transform.position.x) {
			playerDirection = -1.0f;
        }
	}

	void OnTriggerExit2D(Collider2D coll) {
		Debug.Log("...not");
		
		if (coll.tag == "Player") {
			//Vector2.Distance(transform.position.x, player.transform.position.x);
			follow = false;
		}
	}
}
