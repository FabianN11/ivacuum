﻿using UnityEngine;
using System.Collections;

public class CharacterControlTest : MonoBehaviour {

	public float maxSpeed = 10f;
	bool facingRight = true;
    bool facingUp = true;

	public float jumpSpeed;
	private Rigidbody2D rb;
	public int jumpsMax;
	private int jumpsLeft;
	private int grav;
    public Animator Anim;

	// Use this for initialization
	void Start () {
		jumpsLeft = jumpsMax;
		grav = 1;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float move = Input.GetAxis ("Horizontal");

		GetComponent<Rigidbody2D>().velocity = new Vector2 (move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

        if ( move != 0)
        {
            Anim.SetBool("walking", true);
        }
        else
        {
            Anim.SetBool("walking", false  );
        }

		if (move > 0 && !facingRight) 
			Flip ();
     
		else if (move < 0 && facingRight)
			Flip ();

		if ((Input.GetKeyDown (KeyCode.Space)) && (jumpsLeft > 0)) {

			if (grav == 1) {
				GetComponent<Rigidbody2D>().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.y, jumpSpeed);
			    jumpsLeft--;
            }
			else if (grav == -1) {
				GetComponent<Rigidbody2D>().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.y, -jumpSpeed);
				jumpsLeft--;
            }

		} 


		if (Input.GetKeyDown (KeyCode.W)) {
			GetComponent<Rigidbody2D>().gravityScale *= -1;
			grav *= -1;
            FlipY();
		}

		if (Input.GetKeyDown (KeyCode.R)) {
			Physics2D.gravity *= -1;
			grav *= -1;
            FlipY();
		}

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

    }

    void Flip () {
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

    void FlipY()
    {
        facingUp = !facingUp;
        Vector3 theScale = transform.localScale;
        theScale.y *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Ground" || coll.tag == "Enemy")
        {
            Anim.SetBool("inAir", false);
            jumpsLeft = jumpsMax;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Ground")
        {
            Anim.SetBool("inAir", true);
        }
    }

}